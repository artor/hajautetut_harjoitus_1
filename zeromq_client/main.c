#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>

void get_message(char **msg, size_t *size)
{
  printf("Enter text to send to the server.\n");
  printf(">");
  getline(msg,size,stdin);
}

int main (void)
{
    void *context = zmq_ctx_new ();
    void *client = zmq_socket (context, ZMQ_REQ);
    char *msg=NULL;
    size_t size=0;
    char buffer[255];
    get_message(&msg,&size);
   
    printf("Connecting to server...\n");
    zmq_connect(client,"tcp://127.0.0.1:12555");
    zmq_send(client,msg,size,0);
    printf("Awaiting message from server\n");
    zmq_recv(client,buffer,255,0);
    printf("Response from server: %s\n",buffer);

    // Need to free msg as it is allocated by getline()
    if(msg != NULL)    
      free(msg);
    zmq_close(client);
    zmq_ctx_destroy(context);
    return 0;
}


