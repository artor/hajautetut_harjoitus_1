#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

void show_error()
{
  switch(zmq_errno())    
	{
	case EPROTONOSUPPORT:
	  printf("Transport protocol not supported\n");
	  break;
	case ENOCOMPATPROTO:
	  printf("transport protocol is not compatible with socket\n");
	  break;
	case EADDRINUSE:
	  printf("address is already in use\n");
	  break;
	case EADDRNOTAVAIL:
	  printf("address was not local\n");
	  break;
	case ENODEV:
	  printf("address specifies nonexistent interface\n");
	  break;
	case ETERM:
	  printf("context was terminated\n");
	  break;
	case EFAULT:
	  printf("Socket is not valid\n");
	}

}

int main (void)
{
    //  Socket to talk to clients
    void *context = zmq_ctx_new ();
    void *responder = zmq_socket (context, ZMQ_REP);
    int rc = zmq_bind (responder, "tcp://127.0.0.1:12555");
    //assert (rc == 0);
    if(rc != 0)
    {
      show_error();
      return -1;
    }

    while (1) {
        char buffer [255];
        zmq_recv (responder, buffer, 255, 0);
        printf ("Received %s\n",buffer);
        zmq_send (responder, buffer, sizeof(buffer), 0);
    }
    return 0;
}

